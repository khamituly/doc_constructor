package com.example.constructor.models.documents;

import javax.persistence.*;

@Entity
public class UserAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private int userId;
    @Column
    private int docId;
    @Column
    private int questionId;
    @Column
    private String answer;


    public UserAnswer() {
    }

    public UserAnswer(int userId, int docId, int questionId, String answer) {
        this.userId = userId;
        this.docId = docId;
        this.questionId = questionId;
        this.answer = answer;
    }

    public UserAnswer(int userId, int questionId, String answer) {
        this.userId = userId;
        this.questionId = questionId;
        this.answer = answer;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
