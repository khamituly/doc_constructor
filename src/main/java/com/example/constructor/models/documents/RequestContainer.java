package com.example.constructor.models.documents;

public class RequestContainer<T,H,W> {
    private T firstArg;
    private H secondArg;
    private W thirdArg;
    private Integer requestId;

    public RequestContainer(T firstArg, H secondArg,W thirdArg, int requestId) {
        this.firstArg = firstArg;
        this.secondArg = secondArg;
        this.thirdArg = thirdArg;
        this.requestId = requestId;
    }

    public RequestContainer(T firstArg, H secondArg, int requestId){
        this.firstArg = firstArg;
        this.secondArg = secondArg;
        this.requestId = requestId;
        this.thirdArg = null;
    }

    public RequestContainer() {
    }

    public W getThirdArg() {
        return thirdArg;
    }

    public void setThirdArg(W thirdArg) {
        this.thirdArg = thirdArg;
    }

    public T getFirstArg() {
        return firstArg;
    }

    public void setFirstArg(T firstArg) {
        this.firstArg = firstArg;
    }

    public H getSecondArg() {
        return secondArg;
    }

    public void setSecondArg(H secondArg) {
        this.secondArg = secondArg;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
}
