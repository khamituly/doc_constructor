package com.example.constructor.models.documents;

import com.example.constructor.models.users.Role;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Component

public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String fileName;
    @Column
    private Role forWhoIntended;
    @Column
    private String absolutePath;


    public Document() {
    }


    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String name) {
        this.fileName = name;
    }

    public Role getForWhoIntended() {
        return forWhoIntended;
    }

    public void setForWhoIntended(Role forWhoIntended) {
        this.forWhoIntended = forWhoIntended;
    }
}

