package com.example.constructor.models.documents;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Component
public class DocQuestions {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private int docId;
    @Column
    private String target;
    @Column
    private String question;

    public DocQuestions() {
    }

    public DocQuestions(int docId, String target, String question) {
        this.docId = docId;
        this.target = target;
        this.question = question;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

}
