package com.example.constructor.models.users;

public enum Role {
    ADMIN,STUDENT,TEACHER,JURIDIC_PERSON,UNDEFINED;
}
