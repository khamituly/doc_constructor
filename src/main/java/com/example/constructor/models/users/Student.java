package com.example.constructor.models.users;

import javax.persistence.*;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int studentId;
    @Column
    private String firstname;
    @Column
    private String lastname;
    @Column
    private int userId;
    @Column
    private String major;
    @Column
    private String faculty;
    @Column
    private String codeOfMajor;
    @Column
    private long iin;
    @Column
    private String birthdayDate;
    @Column
    private long pasportNum;
    @Column
    private String pasportGivenBy;
    @Column
    private long phoneNumber;
    @Column
    private String pasportWhenGiven;
    @Column
    private String adres;

    public Student() {
    }

    public String getFullname(){
        return this.getLastname()+" "+this.getFirstname();
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getPasportWhenGiven() {
        return pasportWhenGiven;
    }

    public void setPasportWhenGiven(String pasportWhenGiven) {
        this.pasportWhenGiven = pasportWhenGiven;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getStudentId() {
        return studentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getCodeOfMajor() {
        return codeOfMajor;
    }

    public void setCodeOfMajor(String codeOfMajor) {
        this.codeOfMajor = codeOfMajor;
    }

    public long getIin() {
        return iin;
    }

    public void setIin(long iin) {
        this.iin = iin;
    }

    public String getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(String birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public long getPasportNum() {
        return pasportNum;
    }

    public void setPasportNum(long pasportNum) {
        this.pasportNum = pasportNum;
    }

    public String getPasportGivenBy() {
        return pasportGivenBy;
    }

    public void setPasportGivenBy(String pasportGivenBy) {
        this.pasportGivenBy = pasportGivenBy;
    }
}
