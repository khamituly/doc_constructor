package com.example.constructor.models.users;

import javax.persistence.*;

@Entity
public class JuridicPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int juridicPerosnId;
    @Column
    private int userId;
    @Column
    private String organizationName;
    @Column
    private String organizationLeaderName;
    @Column
    private String juridicAdres;
    @Column
    private long bik;
    @Column
    private long bin;
    @Column
    private String bank;
    @Column
    private String kbe;
    @Column
    private String contactInfo;

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public void setJuridicPerosnId(int juridicPerosnId) {
        this.juridicPerosnId = juridicPerosnId;
    }

    public int getJuridicPerosnId() {
        return juridicPerosnId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationLeaderName() {
        return organizationLeaderName;
    }

    public void setOrganizationLeaderName(String organizationLeaderName) {
        this.organizationLeaderName = organizationLeaderName;
    }

    public String getJuridicAdres() {
        return juridicAdres;
    }

    public void setJuridicAdres(String juridicAdres) {
        this.juridicAdres = juridicAdres;
    }

    public long getBik() {
        return bik;
    }

    public void setBik(long bik) {
        this.bik = bik;
    }

    public long getBin() {
        return bin;
    }

    public void setBin(long bin) {
        this.bin = bin;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getKbe() {
        return kbe;
    }

    public void setKbe(String kbe) {
        this.kbe = kbe;
    }

}
