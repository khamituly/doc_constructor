package com.example.constructor.controllers;

import com.example.constructor.models.documents.Document;
import com.example.constructor.models.documents.Request;
import com.example.constructor.models.documents.RequestContainer;
import com.example.constructor.models.documents.Status;
import com.example.constructor.models.users.JuridicPerson;
import com.example.constructor.models.users.Role;
import com.example.constructor.models.users.Student;
import com.example.constructor.models.users.User;
import com.example.constructor.repositories.documents.*;
import com.example.constructor.repositories.users.JuridicPersonRepo;
import com.example.constructor.repositories.users.StudentRepo;
import com.example.constructor.services.FilesService;
import com.example.constructor.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("request")
public class RequestController {
    @Autowired
    private UserService userService;
    @Autowired
    private FilesService filesService;
    @Autowired
    private UserAnswerRepo userAnswerRepo;
    @Autowired
    private DocRepo docRepo;
    @Autowired
    private KeywordRepo keywordRepo;
    @Autowired
    private DocQuestionsRepo docQuestionsRepo;
    @Autowired
    private JuridicPersonRepo juridicPersonRepo;
    @Autowired
    private RequestRepo requestRepo;
    @Autowired
    private StudentRepo studentRepo;


    @GetMapping("/getall")
    public String getRequest(Model model){
        User currentUser = userService.getCurrentUser();
        if(currentUser.getRoles().get(0) == Role.JURIDIC_PERSON) {
            List<RequestContainer<Student,Document,Boolean>> requestContainerList = new ArrayList<>();

            JuridicPerson juridicPerson = juridicPersonRepo.findByUserId(currentUser.getId());
            List<Request> requestList = requestRepo.findRequestByOrgId(juridicPerson.getJuridicPerosnId());

            for(Request request : requestList){
                RequestContainer<Student,Document,Boolean> requestContainer = new RequestContainer<>(studentRepo.findStudentByUserId(request.getUserId()),
                                                                                                          docRepo.findDocumentById(request.getDocId()),
                                                                                                          request.getId());
                requestContainerList.add(requestContainer);
            }
            model.addAttribute("map",requestContainerList);
            return "request";

        }else return "redirect:/";
    }

    @PostMapping("/reject")
    public String rejectRequest(int requestId,
                                Model Model){
            Request request = requestRepo.findRequestById(requestId);
            request.setStatus(Status.REJECTED);
            requestRepo.save(request);
            return "redirect:/";
    }

    @PostMapping("/open")
    public String openRequest(int requestId,
                              Model model){
        Request request = requestRepo.findRequestById(requestId);
        Student student = studentRepo.findStudentByUserId(request.getUserId());
        //TODO change for groupping
        Document document = docRepo.findDocumentById(request.getDocId());

        model.addAttribute("request",request);
        model.addAttribute("student",student);
        model.addAttribute("document",document);

        return "inner_request";
                
    }



}
