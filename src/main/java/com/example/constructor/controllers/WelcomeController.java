package com.example.constructor.controllers;

import com.example.constructor.models.documents.Document;
import com.example.constructor.models.documents.RequestContainer;
import com.example.constructor.models.users.Role;
import com.example.constructor.models.users.User;
import com.example.constructor.repositories.users.UserRepo;
import com.example.constructor.services.FilesService;
import com.example.constructor.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class WelcomeController {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    FilesService filesService;
    @Autowired
    UserService userService;

    @GetMapping("/")
    public String home(Model model) {

        User currentUser = userService.getCurrentUser();
        System.out.println("----------------Current logged in user: "+currentUser.getUsername()+"------------------------------");
        Role role = currentUser.getRoles().get(0);

        List<RequestContainer<Document,Boolean,Boolean>> rejectStatus = filesService.getDocumentWithAllStatus(currentUser);
        model.addAttribute("map",rejectStatus);

        if(role.equals(Role.ADMIN)) {
            model.addAttribute("fileList", filesService.getAllfilenames());
            return "admin_panel";
        }else if(role.equals(Role.STUDENT)){
            return "home_user";
        }else if(role.equals(Role.TEACHER)){
            return "home_user";
        }else if(role.equals(Role.JURIDIC_PERSON)){
            return "home_juridic";
        }else{
            return "select_role";
        }


    }

}
