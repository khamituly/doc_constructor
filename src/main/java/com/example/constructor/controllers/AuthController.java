package com.example.constructor.controllers;

import com.example.constructor.models.documents.Document;
import com.example.constructor.models.documents.RequestContainer;
import com.example.constructor.models.users.*;
import com.example.constructor.repositories.users.JuridicPersonRepo;
import com.example.constructor.repositories.users.StudentRepo;
import com.example.constructor.repositories.users.TeacherRepo;
import com.example.constructor.repositories.users.UserRepo;
import com.example.constructor.services.FilesService;
import com.example.constructor.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;

@Controller
public class AuthController {

    @Autowired
    private TeacherRepo teacherRepo;
    @Autowired
    private StudentRepo studentRepo;
    @Autowired
    JuridicPersonRepo juridicPersonRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private FilesService filesService;
    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String login(){
        return "auth";
    }

    @PostMapping("/registration")
    public String registration(
                               @RequestParam("username") String username,
                               @RequestParam("email") String email,
                               @RequestParam("password") String password,
                               Model model)
    {
        User userFromDb = userRepo.findUserByEmail(email);

        if(userFromDb != null){
            return "auth";
        }else{
            User newUser = new User();
            newUser.setUsername(username);
            newUser.setEmail(email);
            newUser.setActive(true);
            newUser.setPassword(password);
            newUser.setRoles(Collections.singletonList(Role.UNDEFINED));
            System.out.println("-----------------------------------------user with email: "+newUser.getEmail()+" is created");
            userRepo.save(newUser);

            return "auth";
        }

    }

    //TODO make pasportGivenBy Enum, in user side make a choice list
    @PostMapping("/addStudent")
    public String addingStudent(
                                @RequestParam("firstname") String firstname,
                                @RequestParam("lastname") String lastname,
                                @RequestParam("major") String major,
                                @RequestParam("faculty") String faculty,
                                @RequestParam("codeOfMajor") String codeOfMajor,
                                @RequestParam("iin") long iin,
                                @RequestParam("birthdayDate") String birthdayDate,
                                @RequestParam("pasportNum") long pasportNum,
                                @RequestParam("pasportGivenBy") String pasportGivenBy,
                                @RequestParam("phoneNumber") long phoneNumber,
                                @RequestParam("pasportWhenGiven") String pasportWhenGiven,
                                @RequestParam("adres") String adres,
                                Model model) {

        User user = userService.getCurrentUser();
        User updatedUser = new User();
        updatedUser.setId(user.getId());
        updatedUser.setUsername(user.getUsername());
        updatedUser.setActive(true);
        updatedUser.setEmail(user.getEmail());
        updatedUser.setPassword(user.getPassword());
        updatedUser.setRoles(Collections.singletonList(Role.STUDENT));
        userRepo.save(updatedUser);

        Student student = new Student();
        student.setFirstname(firstname);
        student.setLastname(lastname);
        student.setBirthdayDate(birthdayDate);
        student.setFaculty(faculty);
        student.setIin(iin);
        student.setMajor(major);
        student.setCodeOfMajor(codeOfMajor);
        student.setPasportNum(pasportNum);
        student.setUserId(user.getId());
        student.setPasportGivenBy(pasportGivenBy);
        student.setPhoneNumber(phoneNumber);
        student.setPasportWhenGiven(pasportWhenGiven);
        student.setAdres(adres);
        studentRepo.save(student);

        List<RequestContainer<Document,Boolean,Boolean>> rejectStatus = filesService.getDocumentWithAllStatus(updatedUser);
        model.addAttribute("map",rejectStatus);
        return "home_user";

    }

    //TODO make academicDegree enum
    @PostMapping("/addTeacher")
    public String addingTeacher(
                                 @RequestParam("firstname") String firstname,
                                 @RequestParam("lastname") String lastname,
                                 @RequestParam("iin") long iin,
                                 @RequestParam("birthdayDate") String birthdayDate,
                                 @RequestParam("pasportNum") long pasportNum,
                                 @RequestParam("pasportGivenBy") String pasportGivenBy,
                                 @RequestParam("phoneNumber") long phoneNumber,
                                 @RequestParam("pasportWhenGiven") String pasportWhenGiven,
                                 @RequestParam("adres") String adres,
                                 @RequestParam("academicDegree") String academicDegree,
                                 Model model){


        User user = userService.getCurrentUser();
        User updatedUser = new User();
        updatedUser.setId(user.getId());
        updatedUser.setUsername(user.getUsername());
        updatedUser.setActive(true);
        updatedUser.setEmail(user.getEmail());
        updatedUser.setPassword(user.getPassword());
        updatedUser.setRoles(Collections.singletonList(Role.TEACHER));
        userRepo.save(updatedUser);

        Teacher teacher = new Teacher();
        teacher.setAcademicDegree(academicDegree);
        teacher.setAdres(adres);
        teacher.setFirstname(firstname);
        teacher.setLastname(lastname);
        teacher.setBirthdayDate(birthdayDate);
        teacher.setIin(iin);
        teacher.setPasportNum(pasportNum);
        teacher.setPhoneNumber(phoneNumber);
        teacher.setPasportGivenBy(pasportGivenBy);
        teacher.setPasportWhenGiven(pasportWhenGiven);
        teacher.setUserId(user.getId());
        teacherRepo.save(teacher);

        List<RequestContainer<Document,Boolean,Boolean>> rejectStatus = filesService.getDocumentWithAllStatus(updatedUser);
        model.addAttribute("map",rejectStatus);
        return "home_user";
    }

    @PostMapping("/addJuridicPerson")
    public String addingJuridicPerson(
                                      @RequestParam("organizationName") String organizationName,
                                      @RequestParam("organizationLeaderName") String orgazanizationLeaderName,
                                      @RequestParam("juridicAdres") String juridicAdres,
                                      @RequestParam("bik") long bik,
                                      @RequestParam("bin") long bin,
                                      @RequestParam("kbe") String kbe,
                                      @RequestParam("bank") String bank,
                                      @RequestParam("contactInfo") String contactInfo,
                                      Model model){

        User user = userService.getCurrentUser();

        User updatedUser = new User();
        updatedUser.setId(user.getId());
        updatedUser.setUsername(user.getUsername());
        updatedUser.setActive(true);
        updatedUser.setEmail(user.getEmail());
        updatedUser.setPassword(user.getPassword());
        updatedUser.setRoles(Collections.singletonList(Role.JURIDIC_PERSON));
        userRepo.save(updatedUser);

        JuridicPerson juridicPerson = new JuridicPerson();
        juridicPerson.setOrganizationName(organizationName);
        juridicPerson.setOrganizationLeaderName(orgazanizationLeaderName);
        juridicPerson.setJuridicAdres(juridicAdres);
        juridicPerson.setUserId(user.getId());
        juridicPerson.setBank(bank);
        juridicPerson.setBik(bik);
        juridicPerson.setBin(bin);
        juridicPerson.setKbe(kbe);
        juridicPerson.setContactInfo(contactInfo);
        juridicPersonRepo.save(juridicPerson);

        List<RequestContainer<Document,Boolean,Boolean>> rejectStatus = filesService.getDocumentWithAllStatus(updatedUser);
        model.addAttribute("map",rejectStatus);
        return "home_juridic";
    }

}
