package com.example.constructor.controllers;

import com.example.constructor.models.documents.*;
import com.example.constructor.models.users.JuridicPerson;
import com.example.constructor.models.users.Role;
import com.example.constructor.models.users.User;
import com.example.constructor.repositories.documents.*;
import com.example.constructor.repositories.users.JuridicPersonRepo;
import com.example.constructor.repositories.users.StudentRepo;
import com.example.constructor.services.FilesService;
import com.example.constructor.services.UserService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@Controller
public class DocumentController {

    @Autowired
    private UserService userService;
    @Autowired
    private FilesService filesService;
    @Autowired
    private UserAnswerRepo userAnswerRepo;
    @Autowired
    private DocRepo docRepo;
    @Autowired
    private KeywordRepo keywordRepo;
    @Autowired
    private DocQuestionsRepo docQuestionsRepo;
    @Autowired
    private JuridicPersonRepo juridicPersonRepo;
    @Autowired
    private RequestRepo requestRepo;
    @Autowired
    private StudentRepo studentRepo;

    @PostMapping("/addKeyword")
    public String key(@RequestParam("role") String roleS,
                      @RequestParam("keyword")String keywordS){
        Keyword keyword = new Keyword();
        Role role = null;
        switch (roleS){
            case "s":
               role = Role.STUDENT;
               break;
            case "t":
                role = Role.TEACHER;
                break;
            case "j":
                role = Role.JURIDIC_PERSON;
                break;
        }
        keyword.setKeyword(keywordS);
        keyword.setRole(role);
        keywordRepo.save(keyword);

        return "keyword";
    }

    @PostMapping("/docdownload")
    public void download(@RequestParam("filename") String filename,
                         HttpServletResponse response
    ) {
        String path = filesService.getPathToFilledDoc( userService.getCurrentUser(),filename);

        response.setContentType("application/msword");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename"+filename;

        response.setHeader(headerKey,headerValue);
        response.setHeader("Content-Transfer-Encoding","binary");

        try {
            BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
            FileInputStream fis = new FileInputStream(path);
            int len;
            byte[] bytes = new byte[1024];
            while ((len = fis.read(bytes)) >0){
                bos.write(bytes,0,len);
            }
            bos.close();
            response.flushBuffer();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @PostMapping("/uplaodfile")
    public String uploadFile(@RequestParam("file") MultipartFile file,
                             Model model) throws IOException{
        System.out.println("--------------------------------------" + file.getOriginalFilename() + "-------------------------------------------------");
        filesService.uploadFile(file);
        Document document  = filesService.getDocumentByFilename(file.getOriginalFilename());

        model.addAttribute("docId",document.getId());
        return "identifyAddressee";
    }


    @PostMapping("/identifyAddressee")
    public String identifyAddressee(@RequestParam("docId") int docId,
                                 @RequestParam("role") String variant,
                                 Model model) throws IOException, InvalidFormatException {
        Role role = null;
        switch (variant) {
            case "STUDENT":
                role = Role.STUDENT;
                break;
            case "TEACHER":
                role = Role.TEACHER;
                break;
            case "JURIDIC_PERSON":
                role = Role.JURIDIC_PERSON;
                break;
        }

        Document document = filesService.getDocumentById(docId);

        Document updatedDocument = new Document();
        updatedDocument.setId(document.getId());
        updatedDocument.setFileName(document.getFileName());
        updatedDocument.setAbsolutePath(document.getAbsolutePath());
        updatedDocument.setForWhoIntended(role);
        docRepo.save(updatedDocument);

        List<Keyword> exsitingKeywordListInDoc = filesService.findKeywordsFromFiles(new File(updatedDocument.getAbsolutePath()));

        if (exsitingKeywordListInDoc.isEmpty()) {
            System.out.println("document doesn't has any keyword");
        } else {
            for (Keyword keyword : exsitingKeywordListInDoc) {
                String roleString = null;
                switch (keyword.getRole()) {
                    case STUDENT:
                        roleString = "student";
                        break;
                    case TEACHER:
                        roleString = "teacher";
                        break;
                    case JURIDIC_PERSON:
                        roleString = "juridicPerson";
                        break;
                }
                DocQuestions docQuestions = new DocQuestions(docId, keyword.getKeyword(), roleString);
                docQuestionsRepo.save(docQuestions);
            }
        }
            model.addAttribute("docId", docId);
            return "file_questiner";
        }


    @PostMapping("/addquestion")
    public String addQuestion(@RequestParam("docId") int docId,
                              @RequestParam("key") String key,
                              @RequestParam("value") String value,
                              Model model){
        filesService.addQuestionToDoc(docId,key,value);
        model.addAttribute("docId",docId);
        return "file_questiner";
    }


    @PostMapping(value ="openfillform")
    public String openingFillingFormWithChek(@RequestParam("filename") String filename,
                                     Model model) throws IOException, InvalidFormatException {

        Document document = filesService.getDocumentByFilename(filename);
        List<DocQuestions> docQuestionsList = filesService.getDocQuestionsByDocId(document.getId());
        User currentUser = userService.getCurrentUser();

        if(currentUser.getRoles().get(0).equals(Role.STUDENT)){
            for(DocQuestions docQuestions : docQuestionsList){
                if(docQuestions.getQuestion().equals("juridicPerson")) {
                    List<JuridicPerson> juridicPersonList = juridicPersonRepo.findAll();
                    System.out.println(juridicPersonList.size());
                    model.addAttribute("list",juridicPersonList);
                    model.addAttribute("docId",document.getId());
                    return "identifyOrganization";
                }
            }
        }

            List<DocQuestions> remainder = filesService.automaticFill(docQuestionsList);

            if (remainder.isEmpty()) {
                return "redirect:/";
            } else {
                model.addAttribute("question", docQuestionsList.get(0));
                model.addAttribute("count", 0);
                model.addAttribute("document", document);

                return "document_filling";
            }
    }




    @PostMapping(value ="sendrequesttoorg")
    public String openingFillingForm(@RequestParam("docId") int docId,
                                     @RequestParam("organization") int org_id,
                                     Model model) throws IOException, InvalidFormatException {
        Document document = filesService.getDocumentById(docId);
        User currentUser = userService.getCurrentUser();

        Request request = new Request();
        request.setUserId(currentUser.getId());
        request.setDocId(document.getId());
        request.setOrgId(org_id);
        request.setStatus(Status.NOT_READED);
        requestRepo.save(request);

        List<DocQuestions> docQuestionsList = filesService.getDocQuestionsByDocId(document.getId());
        List<DocQuestions> remainder = filesService.automaticFill(docQuestionsList);

        if(remainder.isEmpty()){
            return "redirect:/";
        }else {
            model.addAttribute("question", docQuestionsList.get(0));
            model.addAttribute("count", 0);
            model.addAttribute("document", document);

            return "document_filling";
        }

    }


    @PostMapping("/fill")
    public String fillingDocument(@RequestParam("count") int count,
                                  @RequestParam("answer") String answer,
                                  @RequestParam("document") int docId,
                                  Model model) throws IOException {
        Document document = filesService.getDocumentById(docId);

        List<DocQuestions> docQuestionsList = filesService.getDocQuestionsByDocId(docId);
        docQuestionsList.removeAll(filesService.findKeywordFromList(docQuestionsList));

        DocQuestions docQuestion = docQuestionsList.get(count);
        User currentUser = userService.getCurrentUser();
        UserAnswer userAnswer = new UserAnswer(currentUser.getId(),docId,docQuestion.getId(),answer);
        userAnswerRepo.save(userAnswer);
        count++;

        if(count>=docQuestionsList.size()){

            List<String> files = filesService.getAllfilenames();
            model.addAttribute("fileList", files);

            filesService.fillingDocument(currentUser,document);
            return "redirect:/";
//            Map<Document,Boolean> map= filesService.getDocumentsWithFillingStatus(currentUser);
//            model.addAttribute("map",map);
//            model.addAttribute("isFilled", true);
//            return "home_user";

        }else{
            model.addAttribute("question",docQuestionsList.get(count));
            model.addAttribute("count",count);
            model.addAttribute("document",document);
            return "document_filling";
        }
    }


//    @GetMapping("organization/requests")
//    public String requestsToOrganization(){
//        User user = userService.getCurrentUser();
//        return "#";
//    }


}
