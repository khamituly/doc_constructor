package com.example.constructor.services;

import com.example.constructor.models.documents.Document;
import com.example.constructor.models.documents.*;
import com.example.constructor.models.users.Role;
import com.example.constructor.models.users.Student;
import com.example.constructor.models.users.User;
import com.example.constructor.repositories.documents.*;
import com.example.constructor.repositories.users.JuridicPersonRepo;
import com.example.constructor.repositories.users.StudentRepo;
import com.example.constructor.repositories.users.TeacherRepo;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Component
public class FilesService {
    @Autowired
    private DocRepo docRepo;
    @Autowired
    private DocQuestionsRepo docQuestionsRepo;
    @Autowired
    private UserAnswerRepo userAnswerRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private KeywordRepo keywordRepo;
    @Autowired
    private StudentRepo studentRepo;
    @Autowired
    private TeacherRepo teacherRepo;
    @Autowired
    private RequestRepo requestRepo;

    @Autowired
    private JuridicPersonRepo juridicPersonRepo;

    public void uploadFile(MultipartFile file) throws IOException {
        String absolutePath = "C:\\Users\\Пользователь\\Desktop\\store\\" + file.getOriginalFilename();
        Document document = new Document();
        document.setFileName(file.getOriginalFilename());
        document.setAbsolutePath(absolutePath);
        docRepo.save(document);
        file.transferTo(new File(absolutePath));

    }

    public List<DocQuestions> getDocQuestionsByDocId(int docId){
        return docQuestionsRepo.findByDocId(docId);
    }

    public List<String> getAllfilenames() {
        List<String> filenames = new ArrayList<>();
        for(Document doc : docRepo.findAll()) filenames.add(doc.getFileName());
        return filenames;
    }

    public void addQuestionToDoc(int docId,String key,String value){
        DocQuestions docQuestions = new DocQuestions(docId,key,value);
        docQuestionsRepo.save(docQuestions);
    }

    public Document getDocumentById(int id){
        return docRepo.findDocumentById(id);
    }

    public Document getDocumentByFilename(String filename){
        return docRepo.findDocumentByFileName(filename);
    }

    public void fillingDocument(User user, Document document){
        try {
        int userId = user.getId();
        int docId = document.getId();

        Map<String,String> answers = getAnswerByUserAnsDocIds(userId,docId);

            InputStream inputStream = null;
            XWPFDocument xdocument = new XWPFDocument(OPCPackage.open(document.getAbsolutePath()));

            for (XWPFParagraph p : xdocument.getParagraphs()){

                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        for(String target : answers.keySet()) {
                            String text = r.getText(0);
                            if (text != null && text.contains(target)) {
                                text = text.replace(target, answers.get(target));
                                r.setText(text, 0);
                                r.setBold(true);
                            }
                        }
                    }
                }
        }

            for (XWPFTable tbl : xdocument.getTables()) {
                for (XWPFTableRow row : tbl.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {

                                for(String target : answers.keySet()) {
                                    String text = r.getText(0);
                                    if (text != null && text.contains(target)) {
                                        text = text.replace(target, answers.get(target));
                                        r.setText(text,0);
                                        r.setBold(true);
                                 }
                              }
                            }
                        }
                    }
                }
            }

        xdocument.write(new FileOutputStream("C:\\Users\\Пользователь\\Desktop\\answered\\"+user.getUsername()+"_"+document.getFileName()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    public Map<String,String> getAnswerByUserAnsDocIds(int userId,int docId){
        Map<String,String> res = new HashMap<>();
        List<UserAnswer> userAnswerList = userAnswerRepo.findUserAnswerByUserId(userId);
        List<DocQuestions> docQuestionsList = docQuestionsRepo.findByDocId(docId);

        for(UserAnswer userAnswer : userAnswerList){
            for(DocQuestions docQuestions : docQuestionsList){
                if(userAnswer.getQuestionId() == docQuestions.getId()){
                    res.put(docQuestions.getTarget(), userAnswer.getAnswer());
                }
            }
        }

        return res;
    }

    public List<Keyword> findKeywordsFromFiles(File file) throws InvalidFormatException, IOException {
        List<Keyword> allKeywords = keywordRepo.findAll();
        List<Keyword> result = new ArrayList<>();

        XWPFDocument xdocument = new XWPFDocument(OPCPackage.open(file.getAbsolutePath()));
        for (XWPFParagraph p : xdocument.getParagraphs()){

            List<XWPFRun> runs = p.getRuns();
            if (runs != null) {
                for (XWPFRun r : runs) {

                    for(Keyword keyword : allKeywords) {
                        String text = r.getText(0);
                        if (text != null && text.contains(keyword.getKeyword())) {
                            result.add(keyword);
                        }
                    }
                }
            }
        }

        for (XWPFTable tbl : xdocument.getTables()) {
            for (XWPFTableRow row : tbl.getRows()) {
                for (XWPFTableCell cell : row.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {

                            for(Keyword keyword : allKeywords) {
                                String text = r.getText(0);
                                if (text != null && text.contains(keyword.getKeyword())) {
                                    result.add(keyword);
                                }
                            }
                        }
                    }
                }
            }
        }
            return result;
    }


    public List<DocQuestions> automaticFill(List<DocQuestions> docQuestionsList) throws InvalidFormatException, IOException {
       List<DocQuestions> keywords = findKeywordFromList(docQuestionsList);
       User user = userService.getCurrentUser();

       if(!keywords.isEmpty()){
           Document document = getDocumentById(docQuestionsList.get(0).getDocId());

           XWPFDocument xdocument = new XWPFDocument(OPCPackage.open(document.getAbsolutePath()));
           for (XWPFParagraph p : xdocument.getParagraphs()){

               List<XWPFRun> runs = p.getRuns();
               if (runs != null) {
                   for (XWPFRun r : runs) {
                       for(DocQuestions docQuestion : keywords) {
                           if (isTheySameType(user, docQuestion)) {
                               String text = r.getText(0), value = gettingValueByRoleType(docQuestion);

                               if (text != null && text.contains(docQuestion.getTarget())) {
                                   text = text.replace(docQuestion.getTarget(), value);
                                   r.setText(text, 0);
                                   r.setBold(true);
                                   UserAnswer userAnswer = new UserAnswer(user.getId(), document.getId(), docQuestion.getId(), value);
                                   userAnswerRepo.save(userAnswer);
                               }
                           }else{
                               continue;
                           }
                       }
                   }
               }
           }

           for (XWPFTable tbl : xdocument.getTables()) {
               for (XWPFTableRow row : tbl.getRows()) {
                   for (XWPFTableCell cell : row.getTableCells()) {
                       for (XWPFParagraph p : cell.getParagraphs()) {
                           for (XWPFRun r : p.getRuns()) {
                               for(DocQuestions docQuestion : keywords) {
                                   if (isTheySameType(user, docQuestion)) {
                                       String text = r.getText(0),value = gettingValueByRoleType(docQuestion);
                                       if (text != null && text.contains(docQuestion.getTarget())) {
                                           text = text.replace(docQuestion.getTarget(),value);
                                           r.setText(text, 0);
                                           r.setBold(true);
                                           UserAnswer userAnswer = new UserAnswer(user.getId(),document.getId(),docQuestion.getId(),value);
                                           userAnswerRepo.save(userAnswer);
                                       }
                                  }else{
                                       continue;
                                   }
                               }

                           }
                       }
                   }
               }
           }
           xdocument.write(new FileOutputStream("C:\\Users\\Пользователь\\Desktop\\answered\\"+user.getUsername()+"_"+document.getFileName()));
           docQuestionsList.removeAll(keywords);
       }

       return docQuestionsList;
    }


    public List<RequestContainer<Document,Boolean,Boolean>> getDocumentWithAllStatus(User user){
        List<RequestContainer<Document,Boolean,Boolean>> requestContainerList = new ArrayList<>();

        List<Document> documentList = docRepo.findDocumentsByForWhoIntended(user.getRoles().get(0));

        for(Document document : documentList){
            Request stat = isDocumentRequestAvailableToReject(document,user);
            RequestContainer<Document,Boolean,Boolean> requestContainer = new RequestContainer<>();
            requestContainer.setFirstArg(document);
            if(stat != null){
                requestContainer.setThirdArg(true);// reject status
                requestContainer.setRequestId(stat.getId());
            }else{
                requestContainer.setThirdArg(false); // reject status
                requestContainer.setRequestId(null);
            }

            if(userAnswerRepo.findUserAnswerByUserIdAndDocId(user.getId(),document.getId()).size()>0 && isDocumentAvailableToDownload(document,user)){
                requestContainer.setSecondArg(true); // download status
            }else{
                requestContainer.setSecondArg(false); // download status
            }
            requestContainerList.add(requestContainer);
        }

        return requestContainerList;
    }

    public boolean isDocumentAvailableToDownload(Document document,User user){
        List<Request> requestList = requestRepo.findRequestByDocIdAndUserIdOrderById(document.getId(),user.getId());
        if(requestList.size() != 0){
            Request request = requestList.get(0);
            if (request.getStatus() != Status.APPROVED) {
                return false;
            }
        }
        return true;
    }

    public Request isDocumentRequestAvailableToReject(Document document, User user){
        List<Request> requestList = requestRepo.findRequestByDocIdAndUserIdOrderById(document.getId(),user.getId());
        if(requestList.size() != 0){
                Request request = requestList.get(0);
                if (request.getStatus() == Status.NOT_READED || request.getStatus() == Status.READED) {
                    return request;
                }
        }
        return null;
    }

    public boolean isTheySameType(User user, DocQuestions docQuestions){
        Role role = null;
        switch (docQuestions.getQuestion()){
            case "student":
                role = Role.STUDENT;
                break;
            case "teacher":
                role = Role.TEACHER;
                break;
            case "juridicPerson":
                role = Role.JURIDIC_PERSON;
                break;
        }

        if(role == user.getRoles().get(0)) return true;
        else return false;
    }


    public String gettingValueByRoleType(DocQuestions docQuestions){
        String value = null;
        switch (docQuestions.getQuestion()){
            case "student":
                value = studentKeywords(docQuestions.getTarget());
                break;
            case "teacher":
                value = teacherKeywords(docQuestions.getTarget());
                break;
            case "juridicPerson":
                value = juridicPesonKeywords(docQuestions.getTarget());
                break;
        }
        return value;
    }


    public String studentKeywords(String target){
        String res = " ";
        User currentUser = userService.getCurrentUser();
        Student student = studentRepo.findStudentByUserId(currentUser.getId());
        switch (target) {
            case "sFirstname":
                res = student.getFirstname();
                break;
            case "sLastname":
                res = student.getLastname();
                break;
            case "sMajor":
                res = student.getMajor();
                break;
            case "sFaculty":
                res = student.getFaculty();
                break;
            case "codeOfStudentMajor":
                res = student.getCodeOfMajor();
                break;
            case "sIIN":
                res = String.valueOf(student.getIin());
                break;
            case "sBirthday":
                res = student.getBirthdayDate();
                break;
            case "sPasportNumber":
                res = String.valueOf(student.getPasportNum());
                break;
            case "sPasportGivenBy":
                res = student.getPasportGivenBy();
                break;
            case "sPasportGivenWhen":
                res = student.getPasportWhenGiven();
                break;
            case "sAdres":
                res = student.getAdres();
                break;
            case "sEmail":
                res = currentUser.getEmail();
                break;
            case "sPhoneNumber":
                res = String.valueOf(student.getPhoneNumber());
                break;
        }
        return res;
    }

    public String teacherKeywords(String target){
        return null;
    }

    public String juridicPesonKeywords(String target){
        return null;
    }

    public List<DocQuestions> findKeywordFromList(List<DocQuestions> docQuestionsList){
        List<DocQuestions> keywords = new ArrayList<>();
        for(DocQuestions docQuestions : docQuestionsList){
            if(docQuestions.getQuestion().equals("student") ||docQuestions.getQuestion().equals("teacher") || docQuestions.getQuestion().equals("juridicPerson")) keywords.add(docQuestions);
        }

        return keywords;
    }



    public String getPathToFilledDoc(User user,String filename){
        String path = user.getUsername()+"_"+filename;
        return "C:\\Users\\Пользователь\\Desktop\\answered\\"+path;
    }

}

