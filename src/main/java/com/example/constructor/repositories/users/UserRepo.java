package com.example.constructor.repositories.users;

import com.example.constructor.models.users.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<User, Integer> {
    User findByEmailAndPassword(String email, String password);
    Boolean existsUserByEmail(String email);
    User findUserById(int id);
    User findUserByEmail(String email);
    User findUserByUsername(String username);
    User findUserByUsernameAndPassword(String username, String password);


}
