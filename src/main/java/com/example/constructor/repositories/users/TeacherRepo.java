package com.example.constructor.repositories.users;

import com.example.constructor.models.users.Teacher;
import org.springframework.data.repository.CrudRepository;

public interface TeacherRepo extends CrudRepository<Teacher,Integer> {

}
