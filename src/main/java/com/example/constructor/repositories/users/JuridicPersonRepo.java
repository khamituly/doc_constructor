package com.example.constructor.repositories.users;

import com.example.constructor.models.users.JuridicPerson;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JuridicPersonRepo extends CrudRepository<JuridicPerson, Integer> {
    List<JuridicPerson> findAll();
    JuridicPerson findByOrganizationName(String name);
    JuridicPerson findByUserId(int userId);
}
