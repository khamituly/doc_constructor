package com.example.constructor.repositories.users;

import com.example.constructor.models.users.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepo extends CrudRepository<Student,Integer> {
    Student findStudentByUserId(int userId);
}
