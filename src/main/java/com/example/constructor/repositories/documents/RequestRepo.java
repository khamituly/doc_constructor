package com.example.constructor.repositories.documents;

import com.example.constructor.models.documents.Request;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RequestRepo extends CrudRepository<Request, Integer> {
    List<Request> findRequestByDocIdAndUserIdOrderById(int docId, int userId);
    List<Request> findRequestByOrgId(int orgId);
    Request findRequestById(int requestId);
}
