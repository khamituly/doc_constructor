package com.example.constructor.repositories.documents;

import com.example.constructor.models.documents.DocQuestions;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DocQuestionsRepo extends CrudRepository<DocQuestions,Integer> {
    List<DocQuestions> findByDocId(int docId);
    DocQuestions findByTarget(String key);
}
