package com.example.constructor.repositories.documents;

import com.example.constructor.models.documents.Keyword;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface KeywordRepo extends CrudRepository<Keyword, Integer> {
    List<Keyword> findAll();
}
