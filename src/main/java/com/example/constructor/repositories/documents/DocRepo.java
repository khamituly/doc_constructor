package com.example.constructor.repositories.documents;

import com.example.constructor.models.documents.Document;
import com.example.constructor.models.users.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DocRepo extends CrudRepository<Document, Integer> {
    Document findDocumentById(int id);
    Document findDocumentByFileName(String fileName);
    List<Document> findAll();
    List<Document> findDocumentsByForWhoIntended(Role role);
}
