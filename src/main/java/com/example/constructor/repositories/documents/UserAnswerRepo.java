package com.example.constructor.repositories.documents;

import com.example.constructor.models.documents.UserAnswer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserAnswerRepo extends CrudRepository<UserAnswer,Integer> {
    List<UserAnswer> findUserAnswerByUserId(int userId);
    List<UserAnswer> findUserAnswerByUserIdAndDocId(int userId, int docId);
    void deleteUserAnswerByUserIdAndQuestionId(int userId,int questionId);
}
